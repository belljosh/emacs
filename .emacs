;; -*- lexical-binding: t -*-
(require 'cl)
(require 'advice)
(let ((default-directory "~/.emacs.d/"))
  (normal-top-level-add-subdirs-to-load-path))
(setq package-archives '((("marmalade" . "http://marmalade-repo.org/packages/") t)
                         (("melpa" . "http://melpa.milkbox.net/packages/") t))) 
                                        ;(use-package )
(require 'evil)
(require 'evil-rails)
(require 'evil-matchit)
(require 'evil-operator-comment)
(require 'surround)
(require 'geiser)
(require 'exec-path-from-shell)
                                        ;(require 'quack)
(require 'slime)
(require 'auto-complete-config)
(require 'ido)
(require 'yasnippet)
(require 'paredit)
(require 'ensime)
(require 'company)
(require 'enh-ruby-mode)
(require 'inf-ruby)
(require 'robe)
                                        ;(require 'flymake-jslint)
(require 'flymake-cursor)
(require 'irony)
                                        ;(require 'js-comint)
(require 'tern)
(require 'js2-mode)
(require 'tern-auto-complete)
(require 'multi-term)
(require 'egg)
                                        ;(require 'js-test)
(require 'package)
(require 'markdown-mode)
(require 'icicles)
(require 'readline-complete "~/.emacs.d/vendor/readline-complete.el/readline-complete.el")
(require 's "~/.emacs.d/vendor/s.el/s.el")
(require 'dash-at-point "~/.emacs.d/vendor/dash-at-point/dash-at-point.el")
                                        ;(require 'expand-region "~/.emacs.d/personal/expand-region/expand-region.el")

(add-hook 'js2-mode-hook (lambda () (slime-mode t)))
(add-hook 'js2-mode-hook (lambda () (tern-mode t)))
(add-hook 'js2-mode-hook (lambda () (lintnode-hook)))
(add-hook 'slime-mode-hook 'set-up-slime-ac)
(add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)
(add-hook 'lisp-mode-hook #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook #'enable-paredit-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'scala-mode-hook 'ensime-scala-hook)
(add-hook 'ruby-mode-hook 'robe-mode)
;(add-hook 'after-init-hook 'global-company-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'js2-mode-hook (lambda () (slime-js-minor-mode 1)))
(add-hook 'enh-ruby-mode-hook 'robe-mode)
(add-hook 'enh-ruby-mode-hook 'inf-ruby-minor-mode)
(add-hook 'c++-mode-hook 'c++-hooks)
(add-hook 'c-mode-hook 'c++-hooks)
(remove-hook 'enh-ruby-mode-hook 'erm-define-faces)

(defun c++-hooks ()
  (yas/minor-mode-on)
  (auto-complete-mode 1)
  (when (member major-mode irony-known-modes)
    (irony-mode 1)))
(defun backwards-kill-line () (interactive) (kill-region
					     (point) (progn (beginning-of-line) (point))))
(defun rename-file-and-buffer (new-name)
  (interactive "New name: ")
  (let ((name (buffer-name))
	(filename (buffer-file-name)))
    (if (not filename)
	(message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
	  (message "A buffer named '%s' already exists!" new-name)
	(progn
	  (rename-file name new-name 1)
	  (rename-buffer new-name)
	  (set-visited-file-name new-name)
	  (set-buffer-modified-p nil))))))
(defun highlight-keyword (keyword)
  (lambda ()
    (font-lock-add-keywords nil
			    (('keyword 1 )))))
(defun pretty-lambdas ()
  (font-lock-add-keywords
   nil `(("(\\(lambda\\>\\)"
          (0 (progn (compose-region (match-beginning 1) (match-end 1)
                                    ,(make-char 'greek-iso8859-7 107))
                    nil))))))
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

(define-key term-mode-map (kbd "\C-c\C-c") 'term-interrupt-subjob)
                                        ;(define-key js2-mode-map [f2] 'slime-js-reload)
(define-key emacs-lisp-mode-map "\C-c\C-e" 'eval-buffer)
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)
(global-set-key "\C-ce" 'compile)
(global-set-key "\C-x\C-k" 'backwards-kill-line)
(global-set-key "\C-co" 'ido-find-file)
(global-set-key "\C-c\C-er" 'eval-region)

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(add-hook 'slime-repl-mode-hook 'set-up-slime-ac)
                                        ;(eval-after-load "auto-complete"
                                        ;  (add-to-list 'ac-modes 'slime-repl-mode))
(fset 'yes-or-no-p 'y-or-n-p)

                                        ;(load custom-file 'noerror)
(setq mode-line-format
      (list
       ;; the buffer name; the file name as a tool tip
       '(:eval (propertize "%b" ;'face 'font-lock-keyword-face (buffer-file-name)
                           ))

       ;; line and column
       " "
       (propertize "%02l" 'face 'font-lock-string-face) " "
       (propertize "%02c" 'face 'font-lock-string-face)
       " "

       ;; relative position, size of file
       " " 
       (propertize "%p" 'face 'font-lock-type-face) ;; % above top
       " "
       (propertize "%I" 'face 'font-lock-type-face) ;; size
       " "
                                        ;        ;; the current major mode for the buffer.
       " "
       '(:eval (propertize "%m" 'face 'font-lock-doc-string-face))
       " "
       'minor-mode-alist
       " "
       '(:eval (display-time))
       )) 
(push 'company-robe company-backends)
                                        ;(push 'ac-source-robe ac-sources)


(add-to-list 'auto-mode-alist '("\\.rb$" . enh-ruby-mode))
(add-to-list 'interpreter-mode-alist '("ruby" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-by-copying t)
(setq auto-save-default nil)
(setq inferior-lisp-program "/usr/local/bin/sbcl")
(setq custom-file "~/.emacs-custom.el")
(setq tramp-default-method "ssh")
(setq lintnode-location "~/.emacs.d/personal/lintnode")
(setq lintnode-jslint-excludes (list 'nomen 'undef 'plusplus 'onevar 'white))
(setq inferior-js-program-command "node")
(setq evil-default-cursor '("white" bar))
(setq inferior-js-mode-hook
      (lambda ()
        (ansi-color-for-comint-mode-on)
        (add-to-list 'comint-preoutput-filter-functions
                     (lambda (output)
                       (replace-regexp-in-string ".*1G\.\.\..*5G" "..."
						 (replace-regexp-in-string ".*1G.*3G" "&gt;" output))))))
(setq-default indent-tabs-mode nil)
(setq scroll-margin 1
      scroll-conservatively 0
      scroll-up-aggressively 0.01
      scroll-down-aggressively 0.01)
(setq-default scroll-up-aggressively 0.01
              scroll-down-aggressively 0.01)
(set-face-attribute 'default nil :family "Inconsolata" :height 140)
(setq system-uses-terminfo nil)
(setq vc-handled-backends '(RCS SVN CVS SCCS Bzr Hg Mtn Arch))
(setq ring-bell-function #'ignore)
(setq evil-operator-comment-key "gc")
(setq yas-snippet-dirs
      '(
        "~/.emacs.d/vendor/yasnippet/snippets"
                                        ;        "~/.emacs.d/vendor/yasnippet/yasmate/snippets"
        ))

                                        ;(setq inf-ruby-default-implementation "pry")
                                        ;(setq inf-ruby-first-prompt-pattern "^\\[[0-9]+\\] pry\\((.*)\\)> *")
                                        ;(setq inf-ruby-prompt-pattern "^\\[[0-9]+\\] pry\\((.*)\\)[>*\"'] *")
(package-initialize)
(evil-mode 1)
(load-theme 'monokai t)
(tern-ac-setup) 
(global-surround-mode 1)
(global-auto-complete-mode t)
(ido-mode t)
(yas-global-mode 1)
(slime-setup '(slime-fancy 
	       slime-js 
	       slime-fuzzy))
(ac-config-default)    
(irony-enable 'ac)
(winner-mode 1)
(evil-initial-state 'term-mode 'emacs)
(desktop-save-mode 1)
(pretty-lambdas)
